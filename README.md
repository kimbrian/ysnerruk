# Welcome to Ysnerruk

Ysnerruk enables you to exchange currency at lower cost and at your own terms by enabling a peer-to-peer transfer.

## Contents of Documentation
This documentation consists of two sections which are
1. System Architecture: for technical implementation of the system.
2. User Guide: How to guide for end-users.

## Concepts & Terminologies

Offer: An offer to trade. Similar to advertisement<br />
Maker: An user who posted an Offer.<br/>
Taker: An user who accepted an offer.

# 1. System Architecture

The system is built using Single Page Application architecture following MVC pattern.

## 1.1 Frameworks and Tools

 - Maven for dependency management and build
 - Spring MVC
 - JPA with Hibernate
 - Angular
 - Material design components

## 1.2 Project Modules

<p>Project has a Messaging, Offer, Review and User modules.</p>

### 1.2.1 Offer
This is the main module which handles trading


### 1.2.2 Messaging
The messaging module lets users to communicate regarding offer

### 1.2.3 Review
The review module helps users to flag a fraudulent traders and to recommend legitimate traders

# 1.3 Project structure
Two folders:
- frontend
- api

## 1.3.1 Frontend (View)
This is an Angular single page application which connects to the REST backend.<br />
See frontend/README for more info on how to run it.

## 1.3.2 Backend (Model, Controller)
This is a RESTful api built in Spring boot application.<br />
See module api/README for more info on how to run it.